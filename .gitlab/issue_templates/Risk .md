### Risk Area
<!-- example: Team/Stability , optional-->

### Risk Description
<!-- example: Burn out, as in title -->

### Impact
<!-- example: Low productivity and attrition -->

### Impact level
<!-- select label, 1-low impact 5-high impact -->
/label ~"risk-impact::1" ~"risk-impact::2" ~"risk-impact::3" ~"risk-impact::4" ~"risk-impact::5"

### Probability
<!-- select label, 1-low probability 5-high probability -->
/label ~"risk-probability::1" ~"risk-probability::2" ~"risk-probability::3" ~"risk-probability::4" ~"risk-probability::5" 

### Risk Score
<!-- risk score = impact level multiplied by probability, assign to weight of the issue -->
/weight <value>

### Possible Mitigation
<!-- example: Minimise overloading and blockers -->

<!-- Use risk label to represent the risk in the map -->
/label ~"risk"

<!-- Apply risk category. Use group-map for general risks.  -->
/label ~"risk::group-map" ~"Category:Package Registry" ~"Category:Container Registry" ~"Category:Dependency Proxy" 
